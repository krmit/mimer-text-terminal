import prompt from "promise-prompt";
import { Seq, STYLE, COLOR } from "../terminal-sequences/main";
import { TextData, Answer } from "@mimer/text";

type Status = "stop" | "next" | "question";

export type TerminalOutput = (string | Question)[];
export interface Options {
  status: Status;
  debug: boolean;
  trace: boolean;
  verbose: boolean;
  silent: boolean;
}

interface Question {
  question: string;
}

export async function doTerminal(
  output: TerminalOutput,
  answer: Answer,
  options: Options
): Promise<string> {
  let result = "";
  options.status = "next";
  for (const item of output) {
    if (typeof item === "string") {
      result += item;
    } else {
      if (options.silent) {
        result = "";
      }
      const ans = await prompt(result);
      answer[item.question] = ans;
      result = "";
      options.status = "question";
    }
  }

  if (!options.silent && options.status !== "question") {
    console.log(result);
  }

  if (options.status !== "question") {
    let ans = await prompt("Nästa? ");
    if (["q", "Q", "by", "quit"].includes(ans)) {
      options.status = "stop";
    }
  }
  return result;
}

export function textToTerminal(text: TextData): TerminalOutput {
  let result: TerminalOutput = [];
  let prefix = "";
  prefix = text.list === "unordered" ? "  * " + prefix : prefix;
  let i = 1;
  for (const item of text.data ?? []) {
    if (text.list === "ordered") {
      prefix = `  ${i}. `;
      i++;
    }
    if (typeof item === "string") {
      result.push(prefix + item + (text.list ? "\n" : ""));
    } else {
      if (text.list) {
        item.line;
      }
      result = [
        ...result,
        prefix,
        ...textToTerminal(item),
        text.list ? "\n" : "",
      ];
    }
  }

  if (text.headline) {
    result = [
      Seq(STYLE.bold, STYLE.underline),
      ...result,
      Seq(STYLE.default_font_weight, STYLE.remove_underline),
    ];
    text.line = true;
  }

  switch (text.mood) {
    case "bad":
      result = [Seq(COLOR.red), ...result, Seq(COLOR.default)];
      break;
    case "good":
      result = [Seq(COLOR.green), ...result, Seq(COLOR.default)];
      break;
  }

  switch (text.dataType) {
    case "string":
      result = [Seq(COLOR.cyan), ...result, Seq(COLOR.default)];
      break;
    case "number":
      result = [Seq(COLOR.yellow), ...result, Seq(COLOR.default)];
      break;
  }

  switch (text.notability) {
    case "important":
      result = [Seq(STYLE.bold), ...result, Seq(STYLE.default_font_weight)];
      break;
    case "warning":
      result = [
        Seq(COLOR.red, STYLE.bold),
        ...result,
        Seq(COLOR.default, STYLE.default_font_weight),
      ];
      break;
  }

  if (text.line) {
    result = [...result, "\n"];
  }

  if (text.question !== undefined) {
    result = [
      ...result,
      {
        question: text.question,
      },
    ];
  }
  return result;
}
